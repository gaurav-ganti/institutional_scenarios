#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 17:51:22 2021

@author: gauravganti, znicholls
"""
import pymagicc
import pyam
import aneris 
import os 
import numpy as np  
import numpy.testing as npt 
import pandas as pd 
import scmdata 
import pymagicc 
from silicone.utils import convert_units_to_MtCO2_equiv
import silicone.database_crunchers as cr 

#%% Default gases to infill 

gases_to_infill = [
     'Emissions|CO2|AFOLU',
    'Emissions|N2O',
    'Emissions|CH4',
    'Emissions|SF6',
    'Emissions|HFC|HFC125',
    'Emissions|HFC|HFC134a',
    'Emissions|HFC|HFC143a',
    'Emissions|HFC|HFC227ea',
    'Emissions|HFC|HFC23',
    'Emissions|HFC|HFC32',
    'Emissions|HFC|HFC245ca',
    'Emissions|HFC|HFC43-10',
    'Emissions|C2F6',
    'Emissions|C6F14',
    'Emissions|BC',
    'Emissions|CO',
    'Emissions|NOx',
    'Emissions|OC',
    'Emissions|Sulfur',
    'Emissions|VOC',
    'Emissions|NH3'
]

#%%
infilling_methods = [
    (cr.EqualQuantileWalk, 'eqw'), 
    (cr.QuantileRollingWindows,'qrw'), 
    (cr.RMSClosest, 'rms')
]

#%% Functions to help with aneris

def format_historic_data(hist, emission_list):
    """
    Helper function to format historic data into a format compatible with 
    aneris 

    Parameters
    ----------
    hist : pyam.IamDataFrame
        Historic data
    emission_list : list
        Emissions to be considered

    Returns
    -------
    pd.DataFrame

    """
    hist = (
        hist
        .filter(year = [2010],
                variable = emission_list)
        .timeseries()
        .reset_index()
        .drop(columns = ["model","scenario"])
        .rename(columns = {"variable":"gas","unit":"units"})
    )
    
    hist["sector"] = "all"
    hist = hist.set_index(aneris.utils.df_idx)
    hist.columns = hist.columns.astype(str)
    
    return hist

def format_scenario_data(df_scen):
    
    df_scen = (
        df_scen
        .timeseries()
        .reset_index()
    )
    
    df_scen["gas"] = df_scen["variable"]
    df_scen["sector"] = "all"
    df_scen = (
        df_scen
        .drop(columns = ["model","scenario","variable"])
        .rename(columns = {"unit":"units"})
        .set_index(aneris.utils.df_idx)
    )
    df_scen.columns = df_scen.columns.astype(str)
    
    return df_scen

def format_harmonised_output(df, scenario):
    
    df = df.rename(
        columns = {'gas':'variable','units':'unit'}
    ).drop(columns = ['sector'])
    
    df['model'] = 'IEA_WEM'
    df['scenario'] = scenario

    return pyam.IamDataFrame(df)


#%%

def construct_scen_file(infilled_data, scen_file_name):
    """Function to construct scen file"""
    def rename_variables(v):
        """Format variables to MAGICC variable convention"""
        mapping = {
        'Emissions|HFC|HFC143a':'Emissions|HFC143a',
        'Emissions|HFC|HFC245ca':"Emissions|HFC245fa",
        'Emissions|CO2|AFOLU':'Emissions|CO2|MAGICC AFOLU',
        'Emissions|HFC|HFC43-10': 'Emissions|HFC4310',
        'Emissions|HFC|HFC32':'Emissions|HFC32',
        'Emissions|HFC|HFC125':'Emissions|HFC125',
        'Emissions|HFC|HFC227ea':'Emissions|HFC227ea',
        'Emissions|CO2|Energy and Industrial Processes': 'Emissions|CO2|MAGICC Fossil and Industrial',
        'Emissions|HFC|HFC134a':'Emissions|HFC134a',
        'Emissions|HFC|HFC23':'Emissions|HFC23',
        'Emissions|Sulfur': 'Emissions|SOx',
        'Emissions|VOC':'Emissions|NMVOC'
        }
    
        try:
            return mapping[v]
        except KeyError:
            return v

    def replace_units(u):
        """Replace units for NOx"""
            
        mapping = {
            'Mt NOX/yr': 'Mt NO2 / yr',
        }
        try:
            return mapping[u]
        except KeyError:
            return u

    # Filter out HFC and non HFC - unit conversions
    non_hfc = infilled_data.filter(variable = "Emissions|HFC*", keep=False)
    hfc = infilled_data.filter(variable="Emissions|HFC*")
    
    # Work around for HFC345ca
    hfc["unit"] = hfc["variable"].apply(
        lambda x: 
        "kt {} / yr".format(
            x
            .split("Emissions|HFC|")[-1]
            .replace("-", "")
            .replace("245ca", "245fa")
        )
    )

    
    enCO2 = non_hfc.filter(variable="Emissions|CO2|Energy and Industrial Processes")
    enCO2["unit"] = "Mt CO2/yr"
    non_hfc = non_hfc.filter(
        variable = "Emissions|CO2|Energy and Industrial Processes",
        keep = False
    ).append(enCO2)

    # Create an SCM dataframe 
    scen_scm = scmdata.ScmRun(hfc.append(non_hfc))
    writer = pymagicc.io.MAGICCData(scen_scm)
    vars_not_used = [
        "Emissions|CO2", 
        "Emissions|F-Gases",
        'Emissions|kyotoghg_excl_lulucf'    
    ]
    writer = writer.filter(variable=vars_not_used, keep=False)

    # Check whether all the variables are named appropriately
    expected_names = [
        pymagicc.definitions.convert_magicc7_to_openscm_variables(f"{v}_EMIS") 
        for v in pymagicc.definitions.PART_OF_SCENFILE_WITH_EMISSIONS_CODE_1
    ]

    writer["variable"] = writer["variable"].apply(rename_variables)
    writer["unit"] = writer["unit"].apply(replace_units)
    writer["todo"] = "SET"
        
        
    problematic_names = set(writer["variable"].unique()) - set(expected_names)
    assert not problematic_names, problematic_names

    # Fake the missing CF4 emissions 
    faked = 0 * writer.filter(variable="Emissions|CH4")
    faked["variable"] = "Emissions|CF4"
    faked["unit"] ="kt CF4 / yr"
    writer = writer.append(faked)

    # Unit conversion and cleaning 
    unit_specs = pymagicc.definitions.MAGICC7_EMISSIONS_UNITS.set_index("magicc_variable")
    for v in writer["variable"]:
            magicc_var = pymagicc.definitions.convert_magicc7_to_openscm_variables(v, inverse=True).replace("_EMIS", "")
            magicc_unit = unit_specs.loc[magicc_var, "emissions_unit"]
            writer = writer.convert_unit(magicc_unit, variable=v)
    
    # Now we add the 2000 data from the SR1.5 SCEN files to ensure MAGICC doesn't
    # assume constant emissions between 2000 and the start of the SCEN file
    base_scen_file = os.path.join(
        os.path.dirname(__file__),
        "sr15_scenfiles",
        "scenfiles",
        "IPCCSR15_REMIND-MAgPIE 1.7-3.0_PEP_2C_red_netzero_GAS.SCEN",
    )
    extra_hist = pymagicc.io.MAGICCData(base_scen_file).filter(
        year = range(1, writer["year"].min()),
        region = "World"
    )

    # Overwrite model, scenario names
    extra_hist["scenario"] = writer.get_unique_meta("scenario",True)
    extra_hist["model"] = writer.get_unique_meta("model", True)
    extra_hist["unit"] = extra_hist["unit"].apply(
        lambda x: (
            x
            .replace("134a", "134A")
            .replace("143a", "143A")
            .replace("245fa", "245FA")
            .replace("227ea", "227EA")
        )
    )

    writer = pymagicc.io.MAGICCData(pd.concat(
        [
            extra_hist.timeseries(time_axis="year", meta=writer.timeseries().index.names),
            writer.timeseries(time_axis="year"),
        ],
        axis=1,
    ))

    # Write the scen file    
    writer.write("{}.SCEN".format(scen_file_name), magicc_version=6)
    
#%% Statistics
def add_statistics(stats,data,row,years,
                   compare_years,header, header_change):
    """Helper function to add statistics"""
    stats.add(data[years], header=header, row=row)
    for i,j in compare_years:
        perc_reduction = (data[j]/data[i] - 1) * 100 
        stats.add(perc_reduction, header=header_change, row=row,
                  subheader = '{}-{}'.format(i,j))

#%% Plotting helpers
def make_plot_ready(ts):
    ts.columns = ts.columns.map(lambda x: x.year)
    return ts

def process_temperature_data(df):
    # each ensemble member relative to ref period, then shift
    t = (
        df
        .filter(variable = 'Surface Temperature')
        .relative_to_ref_period_mean(year=range(1986, 2005 + 1))
    )
    t = t + 0.61 # Warming reference period

#     # adjust the median of the distribution, but keep the
#     # spread in the distribution
#     t = (
#         df
#         .filter(variable = 'Surface Temperature')
#         .relative_to_ref_period_mean(year=range(1850, 1900 + 1))
#     )
    
#     eval_period = range(1986, 2005 + 1)
#     for i, s in enumerate(t.groupby("scenario")):
#         if i < 1:
#             current_median = s.filter(year=eval_period).timeseries().mean(axis=1).median()
#         else:
#             check_median = s.filter(year=eval_period).timeseries().mean(axis=1).median()
#             npt.assert_allclose(current_median, check_median, atol=1e-3)
    
#     shift = 0.61 - current_median  # Adjust to AR5 estimate
#     t = t + shift

    xs = make_plot_ready(t.timeseries()).columns

    return t, xs


def get_sr15_scenfiles(sr15_scen_file_dir):
    valid_scens_df = pd.read_csv(os.path.join(sr15_scen_file_dir, "final411scenarios.csv"))
    valid_combos = [tuple(v) for v in valid_scens_df[["model", "scenario"]].values]

    valid_sr15_scen_files = []
    for file in os.listdir(os.path.join(sr15_scen_file_dir, "scenfiles")):
        try:
            model = file.split("_")[1]
            scenario = "_".join(file.split("_")[2:])
        except ValueError:
            if "C-ROADS" in file:
                model = "C-ROADS"
                scenario = "-".join(file.split("-")[2:])
            elif "En-ROADS-96" in file:
                model = "En-ROADS-96"
                scenario = "_".join(file.split("_")[2:])
            elif "WEM" in file:
                model = "WEM"
                scenario = "_".join(file.split("_")[2:])
            else:
                raise ValueError(model)

        model = model.replace("IPCCSR15_", "")
        scenario = scenario.replace(".SCEN", "").replace("_GAS", "")
        valid_combo = True
        if (model, scenario) not in valid_combos:
            valid_combo = False

        if valid_combo:
            valid_sr15_scen_files.append(os.path.join(sr15_scen_file_dir, "scenfiles", file))

    assert len(valid_sr15_scen_files) == 411
    
    return valid_sr15_scen_files
