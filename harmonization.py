#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 12:18:00 2021

@author: gauravganti, znicholls
"""
import aneris 
from aneris import harmonize
from aneris.methods import reduce_ratio
import pandas as pd
from utils import (
    format_historic_data,
    format_scenario_data,
    format_harmonised_output
)

#%% Identify last available year
def identify_last_year(df):
    """Function to identify last available year scenario dataframe"""

    max_year = (
        df
        .columns
        .astype(int)
        .max()
    )
    
    return max_year

#%% Construct methods overrides
def construct_methods_override(scen_to_harm):
    """
    Function to construct methods override for aneris

    Parameters
    ----------
    scen_to_harm : pd.DataFrame
        DESCRIPTION.

    Returns
    -------
    methods_frame : TYPE
        DESCRIPTION.

    """
    method = 'reduce_ratio_{}'.format(
        identify_last_year(scen_to_harm)
    )
    
    nvals = scen_to_harm.shape[0]
    methods_frame = pd.DataFrame({
        'gas': scen_to_harm.index.get_level_values('gas'),
        'sector': scen_to_harm.index.get_level_values('sector'),
        'region': scen_to_harm.index.get_level_values('region'),
        'units': scen_to_harm.index.get_level_values('units'),
        'method': [method] * nvals
    }).set_index(aneris.utils.df_idx).sort_index()
    
    return methods_frame


#%% Harmonize
def harmonize_data(df_hist, df_scen, emission_list):
   
    # Scenarios to loop through 
    scenarios = df_scen.scenario 
    
    # Format historic data for harmonisation 
    hist_for_harmonize = format_historic_data(
        df_hist,
        emission_list
    )
    
    # Define config 
    conf = dict()
    conf['harmonize_year'] = str(2010)
    
    for i, scen in enumerate(scenarios):
        print(
            'Harmonizing scenario: {}'.format(scen)
        )
        
        # Filter scenario to harmonize
        scen_to_harm = format_scenario_data(
            df_scen.filter(
                variable = emission_list,
                scenario = scen
            )    
        )
        
        # Initialise harmonization driver 
        driver = harmonize.Harmonizer(
            scen_to_harm,
            hist_for_harmonize,
            conf
        )
        driver._methods["reduce_ratio_2040"] = lambda df, ratios: reduce_ratio(df, ratios, final_year="2040")

        # Construct methods override
        method_override = construct_methods_override(
            scen_to_harm
        )
        end_year = identify_last_year(scen_to_harm)

        # Harmonize
        harmonized = format_harmonised_output(
            driver.harmonize(overrides=method_override['method']).reset_index(),
            scen
        )

        if i == 0:
            result = harmonized
        else:
            result = result.append(harmonized)

    return result