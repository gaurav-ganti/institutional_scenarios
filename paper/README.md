# README

This folder contains the scripts to generate the figures and tables in the main body of the paper.

# Notebooks 

Each notebook is numbered according to the section in which it appears. Section numbering starts after the introductory section. 

* ```s0_load_sr15_data.ipynb```: Loads and saves SR15 data from the IIASA database
* ```s3_infilled_gas_assessment.ipynb```: 
    * Comparison on energy CO2 and non-CO2 gases across pathways
    * Comparison of different infilling methods
* ```s4_temperature_assessment.ipynb```:
    * Computing and plotting temperature rise above pre-industrial levels 
    * Computing exceedance probability for 1.5C and 2C
* ```s5_mitigation_lever_assessment.ipynb```: 
    * Assessing mitigation levers based on (Warszawski et al., 2021)  
* ```s6_energy_system_characteristics.ipynb``` and ```s6_ensys_statistics.ipynb```:
    * Comparing primary energy, total electricity and shares of different technologies in electricity