# Institutional Scenarios

This repository contains the analysis for the paper:

> Brecha et al., (2021). Institutional “Paris Agreement Compatible” Mitigation Scenarios Evaluated Against the Paris Agreement 1.5°C Goal

For questions about the overall repository, harmonisation and infilling, please contact Gaurav Ganti <gaurav.ganti@climateanalytics.org>.
For questions about running MAGICC, please contact Malte Meinshausen <malte.meinshausen@unimelb.edu.au>, Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org> and Jared Lewis <jared.lewis@climate-energy-college.org>.
For questions about running FaIR, please contact Chris Smith <C.JSmith1@leeds.ac.uk>
For questions about the paper and the analysis more generally, please contact Robert Brecha <robert.brecha@climateanalytics.org>.

## Installation

In your terminal, navigate to the `institutional_scenarios` folder that you have cloned, and type in the following commands. The purpose of setting up the environment is to ensure that 
you have the necessary python packages and versions to run the code in this repository. 
```
$ conda env create -n institutional --file environment.yml
$ conda activate institutional
```

# Code and scripts in the repository

The following sections will guide you through the implementation of the framework outlined in Figure 1 of the paper. Please note that the input scenario data is available from the corresponding 
authors on reasonable request.

<img src="./fig1_workflow.png" 
width = "400" 
height = "500"/>

**Step 1: Harmonisation**
* `harmonization.py`: Contains the harmonization functions with the overrides

**Step 2 and 3: Constant Quantile Extension and Gas Infilling**
These steps are performed in the notebook dedicated to each scenario (documented below):
* ```bp_scenarios.ipynb```: [BP scenarios](https://www.bp.com/en/global/corporate/energy-economics/energy-outlook.html) 
* ```equinor_scenarios.ipynb```: [Equinor scenarios](https://www.equinor.com/en/sustainability/energy-perspectives.html)
* ```shell_scenarios.ipynb```: [Shell scenario](https://www.shell.com/energy-and-innovation/the-energy-future/scenarios/shell-scenario-sky.html)
* ```weo_scenarios.ipynb```: [IEA 2020 scenarios](https://www.iea.org/reports/world-energy-outlook-2020)
* ```iea_sr15_scenarios.ipynb```: [IEA 2021 scenario](https://www.iea.org/reports/net-zero-by-2050)

**Step 4: Climate Assessment**
In this step, the climate assessment is performed in line with SR1.5. The input data to MAGICC is in the `scen_files` folder. 
* ```run_magicc.ipynb```: Run MAGICC6 using MAGICC6's probabilistic set and C4MIP carbon cycle configurations, as used in AR5 and SR1.5 (please contact Malte Meinshausen <malte.meinshausen@unimelb.edu.au;malte.meinshausen@climate-resource.com>, Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org> and Jared Lewis <jared.lewis@climate-energy-college.org;jared.lewis@climate-resource.com> for access to the required configurations).
* ```run_fair.ipynb```: Run FaIR in a probabilistic setup, as used in AR5 and SR1.5 (please contact Chris Smith <> for questions about running FaIR)
* ```categorize_scenarios.ipynb```: Categorize scenarios (SR15 categories) based on temperature outcome 

**Scripts to generate figures and statistics used in the paper**
The necessary scripts sit in the `paper` folder. The following python notebooks contain plotting routines: 
* Plotting Figure 2: `s3_infilled_gas_assessment.ipynb`
* Plotting Figure 3: `s4_temperature_assessment.ipynb`
* Plotting Figure 4: `s6_energy_system_characteristics.ipynb`

## Helper functions 

The following files contain helper functions:
* ```utils.py```
* ```plotting.py```
