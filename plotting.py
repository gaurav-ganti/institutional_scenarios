#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 09:30:28 2021

@author: gauravganti
"""
import matplotlib.pyplot as plt 
from utils import(
    make_plot_ready,
    process_temperature_data    
)
#%% Plot 
def plot_temperature_data(main_data,ax,scen_color):
    """Plotting helper!"""
    for scen, color in scen_color:
        plot_data = main_data.filter(scenario=scen)
        temp, x = process_temperature_data(plot_data)
        
        med = temp.process_over('run_id','median')
        p33 = temp.process_over("run_id", operation="quantile", q=0.33)
        p66 = temp.process_over("run_id", operation="quantile", q=0.66)
        
        print(scen, med.max(1).values, p33.max(1).values, p66.max(1).values)
        
        # Plot median 
        ax.plot(
            x,
            make_plot_ready(med).values.T,
            color = color,
            label = scen
        )
        
        # Fill the 33-66% range
        ax.fill_between(
            x,
            make_plot_ready(p33).values.squeeze(),
            make_plot_ready(p66).values.squeeze(),
            alpha=0.1,
            color=color
        )
        
        # Vetical lines
        ax.axhline(1.5, color='#b0cb68', linestyle='dashed')
        ax.axhline(2, color='#f5e363', linestyle='dashed')
        
        ax.grid(alpha=0.3)
        ax.set_xlim(1950,2100)
        ax.set_ylabel("°C over pre-industrial (1850 - 1900)")
        ax.legend()
    
    #fig.savefig(
    #    save_path,
    #    dpi= 450
    #)   